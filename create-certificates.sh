#!/bin/sh
# Root CA
echo "this password should be at least 64 characters long when using AES256" | tee root-ca-key-password.txt
openssl genrsa -out root-ca-key.pem -aes256 -passout file:root-ca-key-password.txt 4096
openssl req -new -x509 -sha256 -key root-ca-key.pem -passin file:root-ca-key-password.txt -subj "/C=EE/ST=HARJU/L=TALLINN/O=FREDCORP/OU=JAVA/CN=ROOT" -out root-ca.pem -days 730
# Admin cert
openssl genrsa -out admin-key-temp.pem 4096
openssl pkcs8 -inform PEM -outform PEM -in admin-key-temp.pem -topk8 -nocrypt -v1 PBE-SHA1-3DES -out admin-key.pem
openssl req -new -key admin-key.pem -subj "/C=EE/ST=HARJU/L=TALLINN/O=FREDCORP/OU=JAVA/CN=ADMIN" -out admin.csr
openssl x509 -req -in admin.csr -CA root-ca.pem -CAkey root-ca-key.pem -passin file:root-ca-key-password.txt -CAcreateserial -sha256 -out admin.pem -days 730
# Node cert 1
openssl genrsa -out node1-key-temp.pem 4096
openssl pkcs8 -inform PEM -outform PEM -in node1-key-temp.pem -topk8 -nocrypt -v1 PBE-SHA1-3DES -out node1-key.pem
openssl req -new -key node1-key.pem -subj "/C=EE/ST=HARJU/L=TALLINN/O=FREDCORP/OU=JAVA/CN=node1.example.com" -out node1.csr
openssl x509 -req -in node1.csr -CA root-ca.pem -CAkey root-ca-key.pem -passin file:root-ca-key-password.txt -CAcreateserial -sha256 -out node1.pem -days 730
# Client cert
openssl genrsa -out client-key-temp.pem 4096
openssl pkcs8 -inform PEM -outform PEM -in client-key-temp.pem -topk8 -nocrypt -v1 PBE-SHA1-3DES -out client-key.pem
openssl req -new -key client-key.pem -subj "/C=EE/ST=HARJU/L=TALLINN/O=FREDCORP/OU=JAVA/CN=CLIENT" -out client.csr
openssl x509 -req -in client.csr -CA root-ca.pem -CAkey root-ca-key.pem -passin file:root-ca-key-password.txt -CAcreateserial -sha256 -out client.pem -days 730
# Cleanup
rm admin-key-temp.pem
rm admin.csr
rm node1-key-temp.pem
rm node1.csr
rm client-key-temp.pem
rm client.csr
