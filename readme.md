# OpenSearch configuration
https://opensearch.org/docs/latest/security-plugin/configuration/

## TLS
OpenSearch uses TLS in two layers: http and transport (inter-node)<br>
https://opensearch.org/docs/latest/security-plugin/configuration/generate-certificates/

Generate your certificates with create-certificates.sh, `opensearch.yml` is configured to use those certificates. Each node needs their own node certificate and CN on the subject line should be unique for each new node. https://opensearch.org/docs/latest/security-plugin/configuration/tls#configure-node-certificates

**root-ca.pem**: This is the certificate of the root CA that signed all other TLS certificates
**node1.pem**: This is the certificate that this node uses when communicating with other nodes on the transport layer (inter-node traffic)  
**node1-key.pem**: The private key for the node1.pem node certificate
**admin.pem**: This is the admin TLS certificate used when making changes to the security configuration. This certificate gives you full access to the cluster  
**admin-key.pem**: The private key for the admin TLS certificate  

## Authentication

Authentication is determined by config.yml. Described in detail here https://opensearch.org/docs/latest/security-plugin/configuration/configuration/.

Dashboards authenticates itself to OpenSearch with basic authentication, described with "basic_internal_auth_domain". Users will be authenticated with TIM generated JWT, described with "jwt_auth_domain". It will be important to customize the signing key parameter with the key retrieved from https://TIM_URL/jwt/verification-key

## Apply all YAML files 

Start the opensearch node(s) and run securityadmin.sh inside the docker container. This command uses the default certificates:

`./plugins/opensearch-security/tools/securityadmin.sh -cd plugins/opensearch-security/securityconfig/ -icl -nhnv -cacert config/root-ca.pem -cert config/admin.pem -key config/admin-key.pem`